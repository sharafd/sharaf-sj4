package ru.sharaf.selenium.model;

/**
 * Модель объекта "фильм"
 */

public class Film {

	private String id;
	private String imdb;
	private String title;
	private String year;
	private String rating;
	private String aka;
	private String plots;
	private String duration;
	private String format;
	private String cover;
	private String trailerURL;
	private String taglines;
	private String plotOutline;
	private String language;
	private String subtitles;
	private String audio;
	private String video;
	private String country;
	private String genres;
	private String director;
	private String writer;
	private String producer;
	private String music;
	private String cast;
	private String notes;

	
	public String getId() {
		return id;
	}
	public Film setId(String id) {
		this.id = id;
		return this;
	}
	
	public String getImdb() {
		return imdb;
	}
	public Film setImdb(String imdb) {
		this.imdb = imdb;
		return this;
	}
	public String getTitle() {
		return title;
	}
	public Film setTitle(String title) {
		this.title = title;
		return this;
	}
	public String getYear() {
		return year;
	}
	public Film setYear(String year) {
		this.year = year;
		return this;
	}

	public String getAKA() {
		return aka;
	}
	public Film setAKA(String aka) {
		this.aka = aka;
		return this;
	}

	public String getDuration() {
		return duration;
	}
	public Film setDuration(String duration) {
		this.duration = duration;
		return this;
	}

	public String getRating() {
		return rating;
	}
	public Film setRating(String rating) {
		this.rating = rating;
		return this;
	}
	
	public String getFormat() {
		return format;
	}
	public Film setFormat(String format) {
		this.format = format;
		return this;
	}

	public String getCover() {
		return cover;
	}
	public Film setCover(String cover) {
		this.cover = cover;
		return this;
	}

	public String getTearilerURL() {
		return trailerURL;
	}
	public Film setTrailerURL(String trailerURL) {
		this.trailerURL = trailerURL;
		return this;
	}
	
	public String getTaglines() {
		return taglines;
	}
	public Film setTaglines(String taglines) {
		this.taglines = taglines;
		return this;
	}
	
	public String getPlotOutline() {
		return plotOutline;
	}
	public Film setPlotOutline(String plotOutline) {
		this.plotOutline = plotOutline;
		return this;
	}
	
	public String getLanguage() {
		return language;
	}
	public Film setLanguage(String language) {
		this.language = language;
		return this;
	}
	
	public String getSubtitles() {
		return subtitles;
	}
	public Film setSubtitles(String subtitles) {
		this.subtitles = subtitles;
		return this;
	}

	public String getAudio() {
		return audio;
	}
	public Film setAudio(String audio) {
		this.audio = audio;
		return this;
	}
	
	public String getVideo() {
		return video;
	}
	public Film setVideo(String video) {
		this.video = video;
		return this;
	}
	
	public String getCountry() {
		return country;
	}
	public Film setCountry(String country) {
		this.country = country;
		return this;
	}
	
	public String getGenres() {
		return genres;
	}
	public Film setGenres(String genres) {
		this.genres = genres;
		return this;
	}

	public String getDirector() {
		return director;
	}
	public Film setDirector(String director) {
		this.director = director;
		return this;
	}
	
	public String getProducer() {
		return producer;
	}
	public Film setProducer(String producer) {
		this.producer = producer;
		return this;
	}
	
	public String getWriter() {
		return writer;
	}
	public Film setWriter(String writer) {
		this.writer = writer;
		return this;
	}
	
	public String getMusic() {
		return music;
	}
	public Film setMusic(String music) {
		this.music = music;
		return this;
	}

	public String getCast() {
		return cast;
	}
	public Film setCast(String cast) {
		this.cast = cast;
		return this;
	}
	
	public String getNotes() {
		return this.notes;
	}
	
	public Film setNotes(String notes) {
		this.notes = notes;
		return this;
	}
	
	public String getPlots() {
		return this.plots;
	}
	
	public Film setPlots(String plots) {
		this.plots = plots;
		return this;
	}
}
