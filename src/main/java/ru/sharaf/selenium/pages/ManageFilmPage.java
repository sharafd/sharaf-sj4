package ru.sharaf.selenium.pages;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import ru.sharaf.selenium.model.Film;

/**
 * 
 * @author damir
 * Работа со страницами добавления и редактирования данных о фильме
 * В тестируемом проекте поля этих страниц одинаковы
 */

public class ManageFilmPage  extends AnyPage{

	public ManageFilmPage(PageManager pages){
		super(pages);
	 }
	
	@FindBy(id = "imdbsearch")
	private WebElement imdbSearch;
	
	@FindBy(css = "img[alt='Add movie']")
	private WebElement addFilmButton;
	
	@FindBy(css = "img[alt='Save']")
	private WebElement saveButton;
	
	@FindBy(css = "input[type='submit']")
	private WebElement submitButton;
	
	@FindBy(xpath = "//input[@class='required digits error' and @name='year']")
	private WebElement yearFieldRequired;
	
	@FindBy(xpath = "//input[@class='required error' and @name='name']")
	private WebElement titleFieldRequired;
	
	@FindBy(xpath = "//input[@name='name']")
	private WebElement nameField;
	
	@FindBy(xpath = "//input[@name='duration']")
	private WebElement durationField;
	
	@FindBy(xpath = "//input[@name='rating']")
	private WebElement ratingField;
	
	@FindBy(xpath = "//textarea[@name='aka']")
	private WebElement akaField;
	
	@FindBy(xpath = "//input[@name='year']")
	private WebElement yearField;
	
	@FindBy(xpath = "//input[@name='cover']")
	private WebElement coverField;
	
	@FindBy(xpath = "//input[@name='format']")
	private WebElement formatField;
		
	@FindBy(xpath = "//input[@name='trailer']")
	private WebElement trailerURLField;
	
	@FindBy(xpath = "//textarea[@name='notes']")
	private WebElement notesField;
	
	@FindBy(xpath = "//textarea[@name='subtitles']")
	private WebElement subtitlesField;
	
	@FindBy(xpath = "//textarea[@name='audio']")
	private WebElement audioField;
	
	@FindBy(xpath = "//textarea[@name='video']")
	private WebElement videoField;
	
	@FindBy(xpath = "//textarea[@name='country']")
	private WebElement countryField;
	
	@FindBy(xpath = "//textarea[@name='director']")
	private WebElement directorField;

	@FindBy(xpath = "//textarea[@name='writer']")
	private WebElement writerField;

	@FindBy(xpath = "//textarea[@name='producer']")
	private WebElement producerField;

	@FindBy(xpath = "//textarea[@name='music']")
	private WebElement musicField;
	
	@FindBy(xpath = "//textarea[@name='cast']")
	private WebElement castField;
	
	@FindBy(xpath = "//textarea[@name='genres']")
	private WebElement genresField;
	
	@FindBy(xpath = "//textarea[@name='plotoutline']")
	private WebElement plotoutlineField;
	
	@FindBy(xpath = "//textarea[@name='taglines']")
	private WebElement taglinesField;
	
	@FindBy(xpath = "//textarea[@name='languages']")
	private WebElement languageField;
	
	@FindBy(xpath = "//textarea[@name='plots']")
	private WebElement plotsField;
	
	@FindBy(xpath = "//a[text()='Home']")
	private WebElement homeLink;
	 
	public void clickHomeLink(){
		 homeLink.click();
	}
		
	private WebElement getLinkText(Film film){
		return wait.until(presenceOfElementLocated(By.linkText(film.getTitle())));	
	}
	
	public void addMovieFromIMDB(Film film){	
		addFilmButton.click();
		imdbSearch.clear();
		imdbSearch.sendKeys(film.getImdb());
		submitButton.click();
		getLinkText(film).click();
		saveButton.click();
		pages.moviePage.clickHomeLink();
	}
	
	public boolean isTitleFieldNotFilled(){
		try{
			wait.until(presenceOfElementLocated(By.xpath("//input[@class='required error' and @name='name']")));
		    return true;
		} catch (TimeoutException to){
		    return false;
		}    
	}
	
	public boolean isYearFieldNotFilled(){
		try{
			wait.until(presenceOfElementLocated(By.xpath("//input[@class='required digits error' and @name='year']")));
		    return true;
		} catch (TimeoutException to){
		    return false;
		}
	}	

	public boolean isMoviePresent(Film film, String elementClass, String order){
		try{
			wait.until(presenceOfElementLocated(By.xpath("(//div[@class='" + elementClass + "' and @title='"
					+ film.getTitle() + "'])[" + order +"]")));
		    return true;
		} catch (TimeoutException to){
		    return false;
		}
    }	
	
	public void gotoHomeLink(){
		homeLink.click();
	}
	
	public void setTitleField(Film film){
		// Обязятельное поле
		nameField.clear();
		nameField.sendKeys(film.getTitle());
	}

	public void setYearField(Film film){
		// Обязятельное поле
		yearField.clear();
		yearField.sendKeys(film.getYear());
	}

	public void setRatingField(Film film){
		if (film.getRating() != null) {
			ratingField.clear();
			ratingField.sendKeys(film.getRating());
		}	
	}

	public void setAKAField(Film film ){
		if (film.getAKA() != null) {
			akaField.clear();
			akaField.sendKeys(film.getAKA());
		}	
	}

	public void setDurationField(Film film){
		if (film.getDuration() != null) {
			durationField.clear();
			durationField.sendKeys(film.getDuration());
		}	
	}
	
	public void setFormatField(Film film ){
		if (film.getFormat() != null) {
			formatField.clear();
			formatField.sendKeys(film.getFormat());
		}	
	}

	public void setCoverField(Film film ){
		if (film.getCover() != null) {
		//	coverField.clear();
			coverField.sendKeys(film.getCover());
		}	
	}
	
	public void setTrailerURLField(Film film){
		if (film.getTearilerURL() != null) {
			trailerURLField.clear();
			trailerURLField.sendKeys(film.getTearilerURL());
		}	
	}
	
	public void setTaglinesField(Film film){
		if (film.getTaglines() != null) {
			taglinesField.clear();
			taglinesField.sendKeys(film.getTaglines());
		}	
	}
	
	public void setLanguageField(Film film){
		if (film.getLanguage() != null) {
			languageField.clear();
			languageField.sendKeys(film.getLanguage());
		}	
	}

	public void setAudioField(Film film){
		if (film.getAudio() != null) {
			audioField.clear();
			audioField.sendKeys(film.getAudio());
		}	
	}
	
	public void setVideoField(Film film){
		if (film.getVideo() != null) {
			videoField.clear();
			videoField.sendKeys(film.getVideo());
		}	
	}
	
	public void setCountryField(Film film){
		if (film.getCountry() != null) {	
			countryField.clear();
			countryField.sendKeys(film.getCountry());
		}	
	}

	public void setGenresField(Film film){
		if (film.getGenres() != null) {
			genresField.clear();
			genresField.sendKeys(film.getGenres());
		}	
	}

	public void setDirectorField(Film film){
		if (film.getDirector() != null) {
			directorField.clear();
			directorField.sendKeys(film.getDirector());
		}	
	}

	public void setProducerField(Film film){
		if (film.getProducer() != null) {
			producerField.clear();
			producerField.sendKeys(film.getProducer());
		}	
	}
	
	public void setWriterField(Film film){
		if (film.getWriter() != null) {
			writerField.clear();
			writerField.sendKeys(film.getWriter());
		}	
	}
	
	public void setMusicField(Film film){
		if (film.getMusic() != null) {
			musicField.clear();
			musicField.sendKeys(film.getMusic());
		}	
	}

	public void setCastField(Film film){
		if (film.getCast() != null) {	
			castField.clear();
			castField.sendKeys(film.getCast());
		}	
	}
	
	public void setNotesField(Film film){
		if (film.getNotes() != null) {
			notesField.clear();
			notesField.sendKeys(film.getCast());
		}	
	}
	
    public void setPlotOutlineField(Film film){
    	if (film.getPlotOutline() != null) {
    		plotoutlineField.clear();
    		plotoutlineField.sendKeys(film.getPlotOutline());
    	}	
    }
    
    public void setPlotsField(Film film){
    	if (film.getPlots() != null) {
    		plotsField.clear();
    		plotsField.sendKeys(film.getPlots());
    	}	
    }
    
    public void setSubtitllesField(Film film){
    	if (film.getSubtitles() != null) {
    		subtitlesField.clear();
    		subtitlesField.sendKeys(film.getSubtitles());	
    	}	
    }

	public void saveMovie(){
		saveButton.click();
		homeLink.click();
	}
	
	public void clickSaveButton() {
		saveButton.click();
	}
	
	
	public void fillAllFields(Film film){
		setTitleField(film);
	    setAKAField(film);
		setYearField(film);
	    setDurationField(film);
		setRatingField(film);
	    setFormatField(film );
	    setCoverField(film );
	    setTrailerURLField(film);
	    setNotesField(film);
	    setTaglinesField(film);
	    setPlotOutlineField(film);
	    setPlotsField(film);
	    setLanguageField(film);
	    setSubtitllesField(film);
	    setAudioField(film);
	    setVideoField(film);
	    setCountryField(film);
	    setGenresField(film);
	    setDirectorField(film);
	    setWriterField(film);
	    setProducerField(film);
	    setMusicField(film);
	    setCastField(film);
	}
	
	public void fillPageFields(Film film){
		homeLink.click();
		pages.internalPage.addButtonClick();
		fillAllFields(film);
	    saveButton.click();
	}

}
