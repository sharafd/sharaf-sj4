package ru.sharaf.selenium.applogic.implementation;

import org.openqa.selenium.WebElement;

import ru.sharaf.selenium.applogic.FilmHelper;
import ru.sharaf.selenium.model.Film;

/**
 * Реализация FilmHelper
 * 
 */

public class MovieHelper extends DriverBasedHelper implements FilmHelper {

  public MovieHelper(AppManager manager) {
	  super(manager.getWebDriver());
  }

  public boolean isMovieNotFound(){
	  return pages.internalPage.isMovieNotFound();
  }
  
  public void clickHomeLink(){
	 pages.manageFilmPage.gotoHomeLink();
  }

  public void clickAddFilmButton() {
	pages.internalPage.addButtonClick();
  }

  public void fillRequiredFields(Film film) {
	pages.manageFilmPage.setTitleField(film);
	pages.manageFilmPage.setYearField(film);
  }

  public void clickSaveButton() {
	pages.manageFilmPage.saveMovie();
  }
	
  @Override
  public void add(Film film) {
	pages.manageFilmPage.fillPageFields(film);
//	clickSaveButton();
  }

  @Override
  public void addFromIMDB(Film film) {
	 pages.manageFilmPage.addMovieFromIMDB(film);
  }

  @Override
  public void edit(WebElement element, Film film) {
	 element.click();
	 pages.moviePage.clickEditButton();
	 pages.manageFilmPage.fillAllFields(film);
	 pages.manageFilmPage.clickSaveButton();
  }
  
 @Override
 public void delete(WebElement film) {
	 film.click();
	 pages.moviePage.removeMovie();
 }

 @Override
 public void search(Film film) {
	 pages.internalPage.searchFilm(film.getTitle());
 }

@Override
public boolean isMovieFound(Film film) {
	return pages.internalPage.isMovieFound(film.getTitle());
}

@Override
public boolean isYearFieldNotFilled() {
	return pages.manageFilmPage.isYearFieldNotFilled();
}

@Override
public boolean isTitleFieldNotFilled() {
	return pages.manageFilmPage.isTitleFieldNotFilled();
}

@Override
public boolean isMovieFound(Film film, String elementClass, String order) {
	return pages.manageFilmPage.isMoviePresent(film, elementClass, order);
}

@Override
public void gotoInternalPage() {
	pages.moviePage.clickHomeLink();
}

@Override
public WebElement getWebElement(Film film, String elementClass, String order) {
	return pages.internalPage.getWebElement(film, elementClass, order);
}

}
