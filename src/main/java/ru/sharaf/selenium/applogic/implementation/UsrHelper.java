package ru.sharaf.selenium.applogic.implementation;

import ru.sharaf.selenium.applogic.UserHelper;
import ru.sharaf.selenium.model.User;

public class UsrHelper extends DriverBasedHelper implements UserHelper {

  public UsrHelper(AppManager manager) {
    super(manager.getWebDriver());
  }

  @Override
  public void loginAs(User user) {  
    pages.loginPage.ensurePageLoaded()
      .setUsernameField(user)
      .setPasswordField(user)
      .clickSubmitButton();
  }

  @Override
  public boolean isLoggedIn() {
    return pages.internalPage.waitPageLoaded();
  }

  @Override
  public boolean isNotLoggedIn() {
    return pages.loginPage.waitPageLoaded();
  }

  @Override
  public void logout() {
	pages.internalPage.ensurePageLoaded()
	      .clickLogoutLink();
  }

  @Override
  public boolean isLoggedInAs(User user) {
	// TODO Auto-generated method stub
	return false;
  }
  
}
