package ru.sharaf.selenium.applogic;

import ru.sharaf.selenium.model.User;

/**
* Интерфейс для авторизации в каталоге.
* 
*/

public interface UserHelper {

	void loginAs(User user);
	void logout();
	
	boolean isLoggedIn();
	boolean isLoggedInAs(User user);
	boolean isNotLoggedIn();

}
