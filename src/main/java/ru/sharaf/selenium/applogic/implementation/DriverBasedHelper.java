package ru.sharaf.selenium.applogic.implementation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.sharaf.selenium.pages.PageManager;
import ru.sharaf.selenium.util.PropertyLoader;

public class DriverBasedHelper {

  protected WebDriver driver;
  protected WebDriverWait wait;
  protected PageManager pages;

  public DriverBasedHelper(WebDriver driver) {
    this.driver = driver;
    // Таймаут берём из настроек
    wait = new WebDriverWait(driver, PropertyLoader.loadIntProperty("app.wait.timeout"));
    pages = new PageManager(driver);
  }
  
}
