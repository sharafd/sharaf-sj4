package ru.sharaf.selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * 
 * Меиенжер станиц
 * 
 */
public class PageManager {

  private WebDriver driver;

  public LoginPage loginPage;
  public InternalPage internalPage;
  public ManageFilmPage  manageFilmPage;
  public MoviePage moviePage;

  private <T extends Page> T initElements(T page) {
	    PageFactory.initElements(new DisplayedElementLocatorFactory(driver, 10), page);
	    return page;
  }
  
  public PageManager(WebDriver driver) {
    this.driver = driver;
    loginPage = initElements(new LoginPage(this));
    internalPage = initElements(new InternalPage(this));
    manageFilmPage = initElements(new ManageFilmPage(this));
    moviePage = initElements(new MoviePage(this));
  }
  
  public WebDriver getWebDriver() {
    return driver;
  }

}
