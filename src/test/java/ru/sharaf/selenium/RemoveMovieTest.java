package ru.sharaf.selenium;

import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import static org.junit.Assert.*;

import ru.sharaf.selenium.model.Film;

/**
 * 
 * @author damir
 * Удаление записи из базы
 * Будем удалять ту, что без обложки.
 */

public class RemoveMovieTest extends ru.sharaf.selenium.pages.TestBase {
	
  @BeforeMethod
  public void loginIfNeeded(){
	app.getNavigationHelper().gotoInternalPage();
	//  Для независимого запуска тестов  		
	 if (app.getUserHelper().isNotLoggedIn()) {
	      app.loginAsDefaultUser();
	  }
  }
  
  @Test
  public void testRemoveMovie() throws Exception {   
	
	Film film = new Film().setTitle("Аватар");
	// Ищем тот, что без обложки
	WebElement element = app.getFilmHelper().getWebElement(film,"nocover", "1");  
    app.getFilmHelper().delete(element);     
    assertTrue(!app.getFilmHelper().isMovieFound(film, "nocover", "1"));
      
  }

}