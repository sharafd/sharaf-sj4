package ru.sharaf.selenium.applogic;

import org.openqa.selenium.WebElement;

import ru.sharaf.selenium.model.Film;

 /**
 * Интерфейс для работы с фильмами в каталоге.
 */

public interface FilmHelper {

	void add(Film film);
	void addFromIMDB(Film film);
	void edit(WebElement element, Film film);
	void delete(WebElement film);
	void search(Film film);
	
	void gotoInternalPage();
	
	WebElement getWebElement(Film film, String elementClass, String order);
	
	boolean isMovieFound(Film film);
	boolean isMovieNotFound();
	boolean isYearFieldNotFilled();
	boolean isTitleFieldNotFilled();
	boolean isMovieFound(Film film, String elementClass, String order);

}
