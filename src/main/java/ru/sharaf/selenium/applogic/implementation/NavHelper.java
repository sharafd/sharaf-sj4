package ru.sharaf.selenium.applogic.implementation;

import ru.sharaf.selenium.applogic.NavigationHelper;

public class NavHelper extends DriverBasedHelper implements NavigationHelper {

  private String baseUrl;

  public NavHelper(AppManager manager) {
    super(manager.getWebDriver());
    this.baseUrl = manager.getBaseUrl();
  }
  
  @Override
  public void openMainPage() {
    driver.get(baseUrl);
  }

  @Override
  public void openRelativeUrl(String url) {
    driver.get(baseUrl + url);
  }

 @Override
 public void gotoUserProfilePage() {
	// TODO Auto-generated method stub	
 }

 @Override
 public void gotoUserManagementPage() {
	// TODO Auto-generated method stub	
 }

 @Override
 public void gotoLoginPage() {
	openRelativeUrl("/php4dvd/");
 }

@Override
public void gotoInternalPage() {
	openRelativeUrl("/php4dvd/#!/sort/name%20asc/");	
}

@Override
public void gotoEditFilmPage() {
	// TODO Auto-generated method stub
	
}

@Override
public void gotoAddFilmPage() {
	openRelativeUrl("/php4dvd/?go=add");
} 


}
