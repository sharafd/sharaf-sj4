package ru.sharaf.selenium;

import static org.testng.AssertJUnit.assertTrue;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ru.sharaf.selenium.model.Film;

/** 
 * 
 * @author damir
 * Добавляем запись в базу вручную,
 * заодно проверим обязательность заполнения полей
 */

public class AddMovieTest extends ru.sharaf.selenium.pages.TestBase {

  @BeforeMethod
  public void loginIfNeeded(){
	app.getNavigationHelper().gotoInternalPage();
	//  Для независимого запуска тестов  		
    if (app.getUserHelper().isNotLoggedIn()) {
	    app.loginAsDefaultUser();
	 }
  }

  @Test
  public void testSaveBlankFilmTest() throws Exception {
    // Пытаемся сохранить запись без заполнения обязательных полей
	
	  Film film = new Film();
	  
	  app.getFilmHelper().add(film);
      assertTrue(app.getFilmHelper().isTitleFieldNotFilled());
      assertTrue(app.getFilmHelper().isYearFieldNotFilled());
  }   

  @Test
  public void testCheckYearFieldTest() throws Exception {
	
	  Film film = new Film().setTitle("Аватар");
	
	  app.getFilmHelper().add(film);
      assertTrue(app.getFilmHelper().isYearFieldNotFilled());
  }
  
  @Test
  public void testCheckTitleFieldTest() throws Exception {
	
  	  Film film = new Film().setYear("2009");
  	  
  	  app.getFilmHelper().add(film);
      assertTrue(app.getFilmHelper().isTitleFieldNotFilled());
  }
  
  @Test
  public void testAddMovieTest() {

	Film film = new Film()
    // Заполняем доступные поля    
		.setTitle("Аватар")
		.setAKA("Avatar")
		.setYear("2009")
		.setTrailerURL("http://www.imdb.com/video/imdb/vi531039513/?ref_=tt_ov_vi")
		.setNotes("5+")
		.setPlotOutline("N/A")
		.setPlots("N/A")
		.setTaglines("avatar\naction\nfantastic")
		.setLanguage("Russian\nEnglish")
		.setSubtitles("russian")
		.setAudio("AC3\nдублированный")
		.setVideo("FullHD")
		.setCountry("USA")
		.setWriter("James Cameron")
		.setDirector("James Cameron")
		.setProducer("James Cameron")
		.setMusic("James Horner")
		.setCast("http://www.imdb.com/title/tt0499549/fullcredits?ref_=tt_ov_st_sm")
		.setGenres("Action | Adventure | Fantasy")	;
	
    app.getFilmHelper().add(film);
    app.getFilmHelper().gotoInternalPage();
    assertTrue(app.getFilmHelper().isMovieFound(film, "nocover", "1"));
  }
}