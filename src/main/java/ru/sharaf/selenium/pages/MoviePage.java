package ru.sharaf.selenium.pages;

import static org.openqa.selenium.support.ui.ExpectedConditions.alertIsPresent;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author damir
 * Страница просмотра инфо о фильме
 */

public class MoviePage  extends AnyPage {

	@FindBy(css = "img[alt='Remove']")
	private WebElement deleteButton;
	
	@FindBy(css = "img[alt='Edit']")
	private WebElement editButton;
	
	@FindBy(xpath = "//a[text()='Home']")
	private WebElement homeLink;
	 
	public void clickHomeLink(){
		 homeLink.click();
	}
	
	public void clickEditButton(){
		 editButton.click();
	}
	
	public MoviePage(PageManager pages) {
		super(pages);
	}
	
    public void removeMovie(){
		 deleteButton.click();
	     wait.until(alertIsPresent()).accept();
	}
	 
}	