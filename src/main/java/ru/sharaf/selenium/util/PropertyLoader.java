package ru.sharaf.selenium.util;

import java.io.IOException;
import java.util.Properties;

/*
 * Class that extracts properties from the prop file.
 * 
 * @author Sebastiano Armeli-Battana
 */
public class PropertyLoader {

	private static final String PROP_FILE = "/application.properties";

	public static String loadProperty(String name) {
		Properties props = new Properties();
		try {
			props.load(PropertyLoader.class.getResourceAsStream(PROP_FILE));
		} catch (IOException e) {
			e.printStackTrace();
		}

		String value = "";

		if (name != null) {
			value = props.getProperty(name);
		}
		return value;
	}
	/** Загрузка числовых параметров  */
	public static int loadIntProperty(String name) {
		Properties props = new Properties();
		try {
			props.load(PropertyLoader.class.getResourceAsStream(PROP_FILE));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int value = 0;
		
		if (name != null) {
			try {
				value = Integer.parseInt(props.getProperty(name).trim());
			} catch (NumberFormatException e){
				e.printStackTrace();
			}
		}
		return value;
	}
	/** Загрузка булевских параметров. 
	 * @param nane допускает значения 1/0 yes/no true/false в любом регистре.*/
	public static boolean loadBoolProperty(String name) {
		Properties props = new Properties();
		try {
			props.load(PropertyLoader.class.getResourceAsStream(PROP_FILE));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		boolean value = false;
		
		if (name != null) {
			String prop = props.getProperty(name).toLowerCase().trim(); 
			switch (prop) {
				case "yes": {
					prop = "true";
					break;
				}
				case "no": {
					prop = "false";
					break;
				}
				case "1": {
					prop = "true";
					break;
				}
				case "0": {
					prop = "false";
					break;
				}
				default : {
					prop = "false";
					break;
				}
			}
			value = Boolean.parseBoolean(prop);
		}
		return value;
	}
}