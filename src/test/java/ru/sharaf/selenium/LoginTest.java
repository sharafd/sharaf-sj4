package ru.sharaf.selenium;

import static org.testng.AssertJUnit.assertTrue;

import org.testng.annotations.*;

import ru.sharaf.selenium.model.User;

/**
 * Проверка логина. Логин берется из настроек
 */

public class LoginTest extends ru.sharaf.selenium.pages.TestBase {
	
	  @BeforeMethod
	  public void mayBeLogout() {
	    if (app.getUserHelper().isNotLoggedIn()) {
	      return;
	    }
	    app.getUserHelper().logout();
	  }
	
	  @Test
	  public void testLoginFailed() throws Exception {
	    User user = new User().setLogin(app.getAppUsername()).setPassword("wrong");
	    app.getUserHelper().loginAs(user);
	    assertTrue(app.getUserHelper().isNotLoggedIn());
	  }
	  
	  @Test
	  public void testLoginOK() throws Exception {
		 // Логин и пароль берём из application.properties 
	     User user = new User().setLogin(app.getAppUsername()).setPassword(app.getAppPassword()); 
	     app.getUserHelper().loginAs(user);
		 assertTrue(app.getUserHelper().isLoggedIn());
	  }

	}