package ru.sharaf.selenium.applogic;

import org.testng.ITestResult;

/**
 * Интерфeйс менеджера приложений
 */

public interface ApplicationManager {

	UserHelper getUserHelper();
	FilmHelper getFilmHelper();
	NavigationHelper getNavigationHelper();

	void stop();
	void loginAsDefaultUser();
    void takeScreenshot(ITestResult result, String folder, String format);
    
	String getAppUsername();
	String getAppPassword();
	
}

