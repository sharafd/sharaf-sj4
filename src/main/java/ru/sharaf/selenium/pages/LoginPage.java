package ru.sharaf.selenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import ru.sharaf.selenium.model.User;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;
/**
 * Страница логина.
 */
public class LoginPage extends AnyPage {

  public LoginPage(PageManager pages) {
		super(pages);
  }

  @FindBy(name = "username")
  private WebElement usernameField;

  @FindBy(name = "password")
  private WebElement passwordField;

  @FindBy(name = "submit")
  private WebElement submitButton;
  
  public LoginPage setUsernameField(User user) {
	usernameField.clear();  
    usernameField.sendKeys(user.getLogin());
    return this;
  }

  public LoginPage setPasswordField(User user) {
	passwordField.clear();  
    passwordField.sendKeys(user.getPassword());
    return this;
  }

  public void clickSubmitButton() {
    submitButton.click();
  }

  public LoginPage ensurePageLoaded() {
    super.ensurePageLoaded();
    wait.until(presenceOfElementLocated(By.id("loginform")));
    return this;
  }
   
}
