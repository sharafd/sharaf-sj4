package ru.sharaf.selenium;

import static org.testng.AssertJUnit.assertTrue;

import org.testng.annotations.*;

import ru.sharaf.selenium.model.Film;

/**
 * 
 * @author damir
 * Проверка добавления фильма по данным IMDB
 */

public class AddMovieFromIMDBTest extends ru.sharaf.selenium.pages.TestBase {

   @BeforeMethod
   public void loginIfNeeded(){
	app.getNavigationHelper().gotoInternalPage();
	//  Для независимого запуска тестов  		
    if (app.getUserHelper().isNotLoggedIn()) {
	      app.loginAsDefaultUser();
	  }
  }
  
  @Test
  public void testAddMovieFromIMDB() throws Exception {
   
    Film film = new Film().setImdb("tt0499549").setTitle("Аватар");
    
    app.getFilmHelper().addFromIMDB(film);
    // А теперь фильмов 'Аватар' стало два
    assertTrue(app.getFilmHelper().isMovieFound(film, "nocover", "2")); 
  }
 
}