package ru.sharaf.selenium.pages;

import java.util.Properties;

import org.testng.ITestResult;
import org.testng.annotations.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import ru.sharaf.selenium.applogic.ApplicationManager;
import ru.sharaf.selenium.applogic.implementation.AppManager;
import ru.sharaf.selenium.util.PropertyLoader;

public class TestBase {

   protected ApplicationManager app;
   
   private String SCREENSHOT_FOLDER;
   
   final String SCREENSHOT_FORMAT = ".png";
   
   @BeforeSuite
   public void prepareEnvirovment(){
	   // Зачистка базы
	   if (PropertyLoader.loadBoolProperty("app.clear.data")){ 
		   prepareDatabase();
		   System.out.println("prepareEnvirovment: delete from movies;");
	   }   
   }
   
	@BeforeClass
	public void init() {
		app = new AppManager();  
		SCREENSHOT_FOLDER = PropertyLoader.loadProperty("app.screenshots.folder");
	}

	@AfterMethod
	public void setScreenshot(ITestResult result) {
		// снимать ли скриншоты
		if (PropertyLoader.loadBoolProperty("app.take.screenshots")){ 
			app.takeScreenshot(result, SCREENSHOT_FOLDER, SCREENSHOT_FORMAT);
		}	
	}
	
	@AfterSuite
	public void stop() {
	  app.stop();
	}
	
	/** Зачистка базы перед прогоном теста */ 
	public void prepareDatabase(){
		
		String database = PropertyLoader.loadProperty("app.mysql.database");
		String login = PropertyLoader.loadProperty("app.mysql.login");
		String password = PropertyLoader.loadProperty("app.mysql.password");
		String host = PropertyLoader.loadProperty("app.mysql.host");
		
		Properties connInfo = new Properties();
        		 
		connInfo.put("user", login);
		connInfo.put("password", password);
           
        try {
        	Connection conn  = DriverManager.getConnection("jdbc:mysql://"+ host + "/" + database, connInfo);
        	PreparedStatement pstmt = conn.prepareStatement("delete from movies;");
        	if (pstmt.execute()){
        		pstmt.close();
        		conn.close();
        	}   	
        } catch (SQLException e) {
			e.printStackTrace();
        }		
	}	
}
