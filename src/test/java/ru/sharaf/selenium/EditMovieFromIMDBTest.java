package ru.sharaf.selenium;

import static org.testng.AssertJUnit.assertFalse;

import java.io.File;

import org.testng.annotations.*;
import org.openqa.selenium.*;

import ru.sharaf.selenium.model.Film;

/**
 * 
 * @author damir
 * Тест отредактирует запись, добавленную
 * AddMovieFromIMDBTest.  
 * Считается проваленным, если  не нашлась запись с наименованием 'Аватар'
 * без обложки, и таких записей меньше двух.
 * Запускаться должен после AddMovieTest. 
 */

public class EditMovieFromIMDBTest extends ru.sharaf.selenium.pages.TestBase {

  @BeforeMethod
  public void loginIfNeeded(){
	app.getNavigationHelper().gotoInternalPage();
	//  Для независимого запуска тестов  		
	 if (app.getUserHelper().isNotLoggedIn()) {
	      app.loginAsDefaultUser();
	  }
  }
	
  @Test
  public void testEditMovieFromIMDB(){

	File cover = new File("src/test/resources/avatar_cover.jpg");
	Film film = new Film()
		.setTitle("Аватар")
		.setYear("2009")
		.setCover(cover.getAbsolutePath().toString());
    // Ищем фильм "Аватар" без обложки. Нам нужен тот, что из IMDB
	WebElement element = app.getFilmHelper().getWebElement(film,"nocover", "2"); 
	app.getFilmHelper().edit(element, film);
	app.getNavigationHelper().gotoInternalPage();
	// А теперь фильм без обложки должен быть только один
	assertFalse(app.getFilmHelper().isMovieFound(film, "nocover", "2"));
  }
  
}