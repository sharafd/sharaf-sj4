package ru.sharaf.selenium.applogic;

/**
 * Интерфейс для работы со страницами
 */

public interface NavigationHelper {

  void openMainPage();
  void openRelativeUrl(String url);
  void gotoLoginPage();
  void gotoInternalPage();
  void gotoUserProfilePage();
  void gotoUserManagementPage();
  void gotoEditFilmPage();
  void gotoAddFilmPage();

}
