package ru.sharaf.selenium;

import static org.testng.AssertJUnit.assertTrue;

import org.testng.annotations.*;

import ru.sharaf.selenium.model.Film;

/** 
 * 
 * @author damir
 * Проверка поиска фильмов в каталоге
 */

public class FindMovieTest extends ru.sharaf.selenium.pages.TestBase {
    
  @BeforeMethod
  public void loginIfNeeded(){
	app.getNavigationHelper().gotoInternalPage();
	//  Для независимого запуска тестов  		
	 if (app.getUserHelper().isNotLoggedIn()) {
	      app.loginAsDefaultUser();
	  }
  }
	
  @Test
  public void testFindNotExistingMovieTest() { 
	// Этого фильма в базе нет
    Film film = new Film().setTitle("Хоббит");
    app.getFilmHelper().search(film);
    assertTrue( app.getFilmHelper().isMovieNotFound());
  }
  
  @Test
  public void testFindExistingMovieTest() throws Exception {
    // А этот - есть
	Film film = new Film().setTitle("Аватар");
    app.getFilmHelper().search(film);
    assertTrue(app.getFilmHelper().isMovieFound(film));
  } 

}